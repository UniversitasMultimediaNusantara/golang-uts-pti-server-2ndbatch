package main

import (
	"encoding/csv"
	"encoding/json"
	"html/template"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	assetsPath = "./assets/mockup.csv"
	username   = "utspti2ndbatch"
	userpass   = "utspti"
)

type Reply struct {
	Status  int      `json:"status"`
	Message string   `json:"message"`
	Data    []Logger `json:"data"`
}

type Logger struct {
	ID         int    `json:"id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	IpAddress  string `json:"ip_address"`
	MACAddress string `json:"mac_address"`
	URLWebsite string `json:"url_website"`
}

func AuthHandler(w http.ResponseWriter, r *http.Request) {
	authParamUsername, ok := r.URL.Query()["auth_name"]

	if !ok || len(authParamUsername) < 1 {
		//if no param / error user_name
		reply := Reply{http.StatusBadRequest, "No auth_name parameter", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)

		return
	}

	authParamUserpass, ok := r.URL.Query()["auth_pass"]

	if !ok || len(authParamUserpass) < 1 {
		reply := Reply{http.StatusBadRequest, "No auth_pass parameter", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

	if authParamUsername[0] == username && authParamUserpass[0] == userpass {
		reply := Reply{http.StatusOK, "Success", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	} else {
		reply := Reply{http.StatusBadRequest, "Wrong auth_name / auth_pass value", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}
}

func DataHandler(w http.ResponseWriter, r *http.Request) {
	var logger []Logger

	authParamUsername, ok := r.URL.Query()["usercookie"]

	if !ok || len(authParamUsername) < 1 {
		reply := Reply{http.StatusBadRequest, "No usercookie parameter", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	} else if authParamUsername[0] != username {
		reply := Reply{http.StatusBadRequest, "Wrong usercookie parameter", []Logger{}}

		js, err := json.Marshal(reply)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	} else {
		f, err := os.Open(assetsPath)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		defer f.Close()

		lines, err := csv.NewReader(f).ReadAll()

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		for _, line := range lines {
			id, _ := strconv.Atoi(line[0])

			data := Logger{
				ID:         id,
				FirstName:  line[1],
				LastName:   line[2],
				IpAddress:  line[3],
				MACAddress: line[4],
				URLWebsite: line[5],
			}

			logger = append(logger, data)
		}

		reply := Reply{http.StatusOK, "Requested data", logger}

		js, err := json.Marshal(reply)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

func WelcomeHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("./web/template/main.html"))

	err := t.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func main() {
	fs := http.FileServer(http.Dir("./web/static/"))

	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static", fs))

	r.HandleFunc("/", WelcomeHandler)
	r.HandleFunc("/auth", AuthHandler).Methods("GET")
	r.HandleFunc("/reqjson", DataHandler).Methods("GET")
	http.ListenAndServe(":20000", handlers.CORS()(r))
}
